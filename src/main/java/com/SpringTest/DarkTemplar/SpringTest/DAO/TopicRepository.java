package com.SpringTest.DarkTemplar.SpringTest.DAO;

import com.SpringTest.DarkTemplar.SpringTest.Model.Topic;
import org.springframework.data.repository.CrudRepository;

public interface TopicRepository extends CrudRepository<Topic, String> {


}
